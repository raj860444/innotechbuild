(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["web-website-website-module"],{

/***/ "./src/app/web/website/homefooter/homefooter.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/web/website/homefooter/homefooter.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi93ZWJzaXRlL2hvbWVmb290ZXIvaG9tZWZvb3Rlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/web/website/homefooter/homefooter.component.html":
/*!******************************************************************!*\
  !*** ./src/app/web/website/homefooter/homefooter.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pull-right\">\n  Innotech ©{{ copyYear }} All Rights Reserved.\n</div>\n<div class=\"clearfix\"></div>\n"

/***/ }),

/***/ "./src/app/web/website/homefooter/homefooter.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/web/website/homefooter/homefooter.component.ts ***!
  \****************************************************************/
/*! exports provided: HomefooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomefooterComponent", function() { return HomefooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomefooterComponent = /** @class */ (function () {
    function HomefooterComponent() {
        this.copyYear = new Date().getFullYear();
    }
    HomefooterComponent.prototype.ngOnInit = function () {
    };
    HomefooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-homefooter',
            template: __webpack_require__(/*! ./homefooter.component.html */ "./src/app/web/website/homefooter/homefooter.component.html"),
            styles: [__webpack_require__(/*! ./homefooter.component.css */ "./src/app/web/website/homefooter/homefooter.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomefooterComponent);
    return HomefooterComponent;
}());



/***/ }),

/***/ "./src/app/web/website/homeheader/homeheader.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/web/website/homeheader/homeheader.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi93ZWJzaXRlL2hvbWVoZWFkZXIvaG9tZWhlYWRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/web/website/homeheader/homeheader.component.html":
/*!******************************************************************!*\
  !*** ./src/app/web/website/homeheader/homeheader.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- top navigation -->\n<div class=\"top_nav\">\n  <div class=\"nav_menu\">\n    <nav>\n      <div class=\"nav toggle\">\n        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>\n      </div>\n\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li class=\"\">\n          <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n            <img src=\"{{ profileImageName }}\" alt=\"\">{{ name }}\n            <span class=\" fa fa-angle-down\"></span>\n          </a>\n          <ul class=\"dropdown-menu dropdown-usermenu pull-right\">\n            <li><!--<a href=\"javascript:;\"> Profile</a>-->\n              <a [routerLink]=\"['/admin/profile']\"> Profile</a>\n            </li>\n            <li>\n              <a [routerLink]=\"['/admin/change-password']\">\n                Change Password\n              </a>\n            </li>\n            <li><a href=\"javascript:;\">Help</a></li>\n            <li><a (click)=\"_authService.logoutUser()\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>\n          </ul>\n        </li>\n\n        <li role=\"presentation\" class=\"dropdown\">\n          <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n            <i class=\"fa fa-envelope-o\"></i>\n            <span class=\"badge bg-green\">6</span>\n          </a>\n          <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list\" role=\"menu\">\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <div class=\"text-center\">\n                <a>\n                  <strong>See All Alerts</strong>\n                  <i class=\"fa fa-angle-right\"></i>\n                </a>\n              </div>\n            </li>\n          </ul>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/web/website/homeheader/homeheader.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/web/website/homeheader/homeheader.component.ts ***!
  \****************************************************************/
/*! exports provided: HomeheaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeheaderComponent", function() { return HomeheaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");



var HomeheaderComponent = /** @class */ (function () {
    function HomeheaderComponent(_authService) {
        this._authService = _authService;
    }
    HomeheaderComponent.prototype.ngOnInit = function () {
    };
    HomeheaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-homeheader',
            template: __webpack_require__(/*! ./homeheader.component.html */ "./src/app/web/website/homeheader/homeheader.component.html"),
            styles: [__webpack_require__(/*! ./homeheader.component.css */ "./src/app/web/website/homeheader/homeheader.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HomeheaderComponent);
    return HomeheaderComponent;
}());



/***/ }),

/***/ "./src/app/web/website/homesidebar/homesidebar.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/web/website/homesidebar/homesidebar.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi93ZWJzaXRlL2hvbWVzaWRlYmFyL2hvbWVzaWRlYmFyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/web/website/homesidebar/homesidebar.component.html":
/*!********************************************************************!*\
  !*** ./src/app/web/website/homesidebar/homesidebar.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- menu profile quick info -->\n<div class=\"profile clearfix\">\n  <div class=\"profile_pic\">\n    <img src=\"{{ profileImageName }}\" alt=\"...\" class=\"img-circle profile_img\">\n  </div>\n  <div class=\"profile_info\">\n    <span>Welcome,</span>\n    <h2>{{ name }}</h2>\n  </div>\n</div>\n<!-- /menu profile quick info -->\n\n<br />\n<div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">\n  <div class=\"menu_section\">\n    <h3>General</h3>\n    <ul class=\"nav side-menu\" data-widget=tree>\n      <li><a><i class=\"fa fa-home\"></i> Home <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          \n\t      <li><a><i class=\"fa fa-language\"></i> Job <span class=\"fa fa-chevron-down\"></span></a>\n\t        <ul class=\"nav child_menu\">\n\t          <li><a routerLink=\"/admin/cms/list\">Job List</a></li>\n\t          \n\t        </ul>\n\t      </li>\n        </ul>\n    </ul>\n  </div>\n\n\n</div>\n<!-- /sidebar menu -->\n\n<!-- /menu footer buttons -->\n<div class=\"sidebar-footer hidden-small\">\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">\n    <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">\n    <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">\n    <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" (click)=\"_authService.logoutUser()\">\n    <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>\n  </a>\n</div>\n\n"

/***/ }),

/***/ "./src/app/web/website/homesidebar/homesidebar.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/web/website/homesidebar/homesidebar.component.ts ***!
  \******************************************************************/
/*! exports provided: HomesidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomesidebarComponent", function() { return HomesidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");




var HomesidebarComponent = /** @class */ (function () {
    function HomesidebarComponent(_authService) {
        this._authService = _authService;
        this.name = localStorage.getItem('name');
        this.profileImageName = localStorage.getItem('profileImageName');
    }
    HomesidebarComponent.prototype.ngDoCheck = function () {
        var minHeight = jquery__WEBPACK_IMPORTED_MODULE_2__(document).height();
        var maxHeight = jquery__WEBPACK_IMPORTED_MODULE_2__(document).height();
        jquery__WEBPACK_IMPORTED_MODULE_2__('.left_col').css("min-height", minHeight);
        //  $('.left_col').css("max-height", maxHeight);
    };
    HomesidebarComponent.prototype.ngOnInit = function () {
        var CURRENT_URL = window.location.href.split('#')[0].split('?')[0], $BODY = jquery__WEBPACK_IMPORTED_MODULE_2__('body'), $MENU_TOGGLE = jquery__WEBPACK_IMPORTED_MODULE_2__('#menu_toggle'), $SIDEBAR_MENU = jquery__WEBPACK_IMPORTED_MODULE_2__('#sidebar-menu'), $SIDEBAR_FOOTER = jquery__WEBPACK_IMPORTED_MODULE_2__('.sidebar-footer'), $LEFT_COL = jquery__WEBPACK_IMPORTED_MODULE_2__('.left_col'), $RIGHT_COL = jquery__WEBPACK_IMPORTED_MODULE_2__('.right_col'), $NAV_MENU = jquery__WEBPACK_IMPORTED_MODULE_2__('.nav_menu'), $FOOTER = jquery__WEBPACK_IMPORTED_MODULE_2__('footer');
        // Sidebar
        jquery__WEBPACK_IMPORTED_MODULE_2__(document).ready(function () {
            jquery__WEBPACK_IMPORTED_MODULE_2__("body").addClass("nav-md");
            // TODO: This is some kind of easy fix, maybe we can improve this
            var setContentHeight = function () {
                // reset height
                $RIGHT_COL.css('min-height', jquery__WEBPACK_IMPORTED_MODULE_2__(window).height());
                var bodyHeight = $BODY.outerHeight(), footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(), leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(), contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
                // normalize content
                contentHeight -= $NAV_MENU.height() + footerHeight;
                $RIGHT_COL.css('min-height', contentHeight);
            };
            $SIDEBAR_MENU.find('a').on('click', function (ev) {
                var $li = jquery__WEBPACK_IMPORTED_MODULE_2__(this).parent();
                if ($li.is('.active')) {
                    $li.removeClass('active active-sm');
                    jquery__WEBPACK_IMPORTED_MODULE_2__('ul:first', $li).slideUp(function () {
                        setContentHeight();
                    });
                }
                else {
                    // prevent closing menu if we are on child menu
                    if (!$li.parent().is('.child_menu')) {
                        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        $SIDEBAR_MENU.find('li ul').slideUp();
                    }
                    $li.addClass('active');
                    jquery__WEBPACK_IMPORTED_MODULE_2__('ul:first', $li).slideDown(function () {
                        setContentHeight();
                    });
                }
            });
            // toggle small or large menu
            $MENU_TOGGLE.on('click', function () {
                if ($BODY.hasClass('nav-md')) {
                    $SIDEBAR_MENU.find('li.active ul').hide();
                    $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                }
                else {
                    $SIDEBAR_MENU.find('li.active-sm ul').show();
                    $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                }
                $BODY.toggleClass('nav-md nav-sm');
                setContentHeight();
                jquery__WEBPACK_IMPORTED_MODULE_2__('.dataTable').each(function () { jquery__WEBPACK_IMPORTED_MODULE_2__(this).dataTable().fnDraw(); });
            });
            // check active menu
            $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');
            $SIDEBAR_MENU.find('a').filter(function () {
                return this.href == CURRENT_URL;
            }).parent('li').addClass('current-page').parents('ul').slideDown(function () {
                setContentHeight();
            }).parent().addClass('active');
            setContentHeight();
            // fixed sidebar
            if (jquery__WEBPACK_IMPORTED_MODULE_2__["fn"].mCustomScrollbar) {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.menu_fixed').mCustomScrollbar({
                    autoHideScrollbar: true,
                    theme: 'minimal',
                    mouseWheel: { preventDefault: true }
                });
            }
        });
        // /Sidebar
    };
    HomesidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-homesidebar',
            template: __webpack_require__(/*! ./homesidebar.component.html */ "./src/app/web/website/homesidebar/homesidebar.component.html"),
            styles: [__webpack_require__(/*! ./homesidebar.component.css */ "./src/app/web/website/homesidebar/homesidebar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], HomesidebarComponent);
    return HomesidebarComponent;
}());



/***/ }),

/***/ "./src/app/web/website/pages/website/website.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/web/website/pages/website/website.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi93ZWJzaXRlL3BhZ2VzL3dlYnNpdGUvd2Vic2l0ZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/web/website/pages/website/website.component.html":
/*!******************************************************************!*\
  !*** ./src/app/web/website/pages/website/website.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container body\">\n<div class=\"main_container\">\n  <div class=\"col-md-3 left_col\">\n    <div class=\"left_col scroll-view\">\n      <div class=\"navbar nav_title\" style=\"border: 0;\">\n        <a href=\"index.html\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>Innotech</span></a>\n      </div>\n      <div class=\"clearfix\"></div>\n      <app-homesidebar></app-homesidebar>\n      <!-- sidebar menu -->\n    </div>\n  </div>\n  <app-homeheader></app-homeheader>\n\n  <router-outlet></router-outlet>\n  <!-- footer content -->\n  <footer>\n    <app-homefooter></app-homefooter>\n  </footer>\n  <!-- /footer content -->\n</div>\n</div>"

/***/ }),

/***/ "./src/app/web/website/pages/website/website.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/web/website/pages/website/website.component.ts ***!
  \****************************************************************/
/*! exports provided: WebsiteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsiteComponent", function() { return WebsiteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WebsiteComponent = /** @class */ (function () {
    function WebsiteComponent() {
    }
    WebsiteComponent.prototype.ngOnInit = function () {
        this.user = JSON.parse(localStorage.getItem('user'));
        console.log(this.user);
    };
    WebsiteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-website',
            template: __webpack_require__(/*! ./website.component.html */ "./src/app/web/website/pages/website/website.component.html"),
            styles: [__webpack_require__(/*! ./website.component.css */ "./src/app/web/website/pages/website/website.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WebsiteComponent);
    return WebsiteComponent;
}());



/***/ }),

/***/ "./src/app/web/website/website-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/web/website/website-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: WebsiteRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsiteRoutingModule", function() { return WebsiteRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_website_website_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/website/website.component */ "./src/app/web/website/pages/website/website.component.ts");




var routes = [{ path: '', component: _pages_website_website_component__WEBPACK_IMPORTED_MODULE_3__["WebsiteComponent"] }];
var WebsiteRoutingModule = /** @class */ (function () {
    function WebsiteRoutingModule() {
    }
    WebsiteRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], WebsiteRoutingModule);
    return WebsiteRoutingModule;
}());



/***/ }),

/***/ "./src/app/web/website/website.module.ts":
/*!***********************************************!*\
  !*** ./src/app/web/website/website.module.ts ***!
  \***********************************************/
/*! exports provided: WebsiteModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsiteModule", function() { return WebsiteModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _website_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./website-routing.module */ "./src/app/web/website/website-routing.module.ts");
/* harmony import */ var _pages_website_website_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/website/website.component */ "./src/app/web/website/pages/website/website.component.ts");
/* harmony import */ var _homesidebar_homesidebar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./homesidebar/homesidebar.component */ "./src/app/web/website/homesidebar/homesidebar.component.ts");
/* harmony import */ var _homeheader_homeheader_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./homeheader/homeheader.component */ "./src/app/web/website/homeheader/homeheader.component.ts");
/* harmony import */ var _homefooter_homefooter_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./homefooter/homefooter.component */ "./src/app/web/website/homefooter/homefooter.component.ts");








var WebsiteModule = /** @class */ (function () {
    function WebsiteModule() {
    }
    WebsiteModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_pages_website_website_component__WEBPACK_IMPORTED_MODULE_4__["WebsiteComponent"], _homesidebar_homesidebar_component__WEBPACK_IMPORTED_MODULE_5__["HomesidebarComponent"], _homeheader_homeheader_component__WEBPACK_IMPORTED_MODULE_6__["HomeheaderComponent"], _homefooter_homefooter_component__WEBPACK_IMPORTED_MODULE_7__["HomefooterComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _website_routing_module__WEBPACK_IMPORTED_MODULE_3__["WebsiteRoutingModule"]
            ]
        })
    ], WebsiteModule);
    return WebsiteModule;
}());



/***/ })

}]);
//# sourceMappingURL=web-website-website-module.js.map