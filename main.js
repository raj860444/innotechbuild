(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"admin-admin-module"
	],
	"./admission/admission.module": [
		"./src/app/admin/admission/admission.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"admission-admission-module"
	],
	"./branch/branch.module": [
		"./src/app/admin/branch/branch.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"branch-branch-module"
	],
	"./cms/cms.module": [
		"./src/app/admin/cms/cms.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"default~cms-cms-module~job-job-module~user-user-module",
		"default~cms-cms-module~job-job-module",
		"cms-cms-module"
	],
	"./contacts/contacts.module": [
		"./src/app/admin/contacts/contacts.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"contacts-contacts-module"
	],
	"./currency/currency.module": [
		"./src/app/admin/currency/currency.module.ts",
		"currency-currency-module"
	],
	"./job/job.module": [
		"./src/app/admin/job/job.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"default~cms-cms-module~job-job-module~user-user-module",
		"default~cms-cms-module~job-job-module",
		"job-job-module"
	],
	"./user/user.module": [
		"./src/app/admin/user/user.module.ts",
		"default~admin-admin-module~admission-admission-module~branch-branch-module~cms-cms-module~contacts-c~8f3aa3dc",
		"default~cms-cms-module~job-job-module~user-user-module",
		"user-user-module"
	],
	"./web/website/website.module": [
		"./src/app/web/website/website.module.ts",
		"web-website-website-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, routingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingComponent", function() { return routingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _guards_role_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./guards/role.guard */ "./src/app/guards/role.guard.ts");
/* harmony import */ var _web_weblogin_weblogin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./web/weblogin/weblogin.component */ "./src/app/web/weblogin/weblogin.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/forgot-password/forgot-password.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./reset-password/reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");











var routes = [
    { path: '', component: _web_weblogin_weblogin_component__WEBPACK_IMPORTED_MODULE_5__["WebloginComponent"] },
    { path: 'home', canActivate: [_guards_role_guard__WEBPACK_IMPORTED_MODULE_4__["RoleGuard"]], data: { role: '1' }, loadChildren: './web/website/website.module#WebsiteModule' },
    { path: 'register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"] },
    { path: 'admin', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'forgot-password', component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_8__["ForgotPasswordComponent"] },
    { path: 'reset-password/:token', component: _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__["ResetPasswordComponent"] },
    { path: 'admin', canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]], loadChildren: './admin/admin.module#AdminModule' },
    { path: '**', component: _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_7__["PageNotFoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

var routingComponent = [_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"], _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_7__["PageNotFoundComponent"]];


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<ng-container *ngIf=\"login;else second\">\n  <div class=\"container body\">\n        <div class=\"main_container\">\n          <div class=\"col-md-3 left_col\">\n            <div class=\"left_col scroll-view\">\n              <div class=\"navbar nav_title\" style=\"border: 0;\">\n                <a href=\"index.html\" class=\"site_title\"><img src=\"./assets/images/innologo.png\" width=\"50pxpx\" height=\"30px\"><span>Innotech</span></a>\n              </div>\n              <div class=\"clearfix\"></div>\n              <app-sidenav></app-sidenav>\n              <!-- sidebar menu -->\n            </div>\n          </div>\n          <app-topnav></app-topnav>\n\n          <router-outlet></router-outlet>\n          <!-- footer content -->\n          <footer>\n            <app-footer></app-footer>\n          </footer>\n          <!-- /footer content -->\n        </div>\n      </div>\n</ng-container>\n<ng-template #second>\n  <router-outlet></router-outlet>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(_router) {
        this._router = _router;
        this.title = 'admin';
        this.login = 0;
    }
    AppComponent.prototype.ngOnInit = function () {
        //console.log(this._router.url)
        if (!!localStorage.getItem('token') && this._router.url !== "/") {
            this.login = 1;
            //this._router.navigate(['/admin/dashboard'])
        }
        else {
            this.login = 0;
            //console.log(localStorage.getItem('token'))
        }
        //console.log('On ng'+ this.login)
    };
    AppComponent.prototype.ngDoCheck = function () {
        //console.log('On ng do check '+ this._router.url)
        if (!!localStorage.getItem('token') && this._router.url !== "/login") {
            this.login = 1;
        }
        else {
            this.login = 0;
            //console.log(localStorage.getItem('token'))
        }
        //console.log('On ng do check'+ this.login)
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: getAuthServiceConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAuthServiceConfigs", function() { return getAuthServiceConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sidenav/sidenav.component */ "./src/app/sidenav/sidenav.component.ts");
/* harmony import */ var _topnav_topnav_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./topnav/topnav.component */ "./src/app/topnav/topnav.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
/* harmony import */ var _guards_role_guard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./guards/role.guard */ "./src/app/guards/role.guard.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _token_interceptor_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./token-interceptor.service */ "./src/app/token-interceptor.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/forgot-password/forgot-password.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./reset-password/reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./logout/logout.component */ "./src/app/logout/logout.component.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _homedash_homedash_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./homedash/homedash.component */ "./src/app/homedash/homedash.component.ts");
/* harmony import */ var _web_webfooter_webfooter_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./web/webfooter/webfooter.component */ "./src/app/web/webfooter/webfooter.component.ts");
/* harmony import */ var _web_weblogin_weblogin_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./web/weblogin/weblogin.component */ "./src/app/web/weblogin/weblogin.component.ts");
















//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';









// Configs 
function getAuthServiceConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__["FacebookLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__["FacebookLoginProvider"]("384699642354567")
        }
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_6__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_7__["FooterComponent"],
                _logout_logout_component__WEBPACK_IMPORTED_MODULE_20__["LogoutComponent"],
                _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_8__["SidenavComponent"],
                _topnav_topnav_component__WEBPACK_IMPORTED_MODULE_9__["TopnavComponent"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["routingComponent"],
                _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_17__["ForgotPasswordComponent"],
                _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_18__["ResetPasswordComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_19__["LoginComponent"],
                _homedash_homedash_component__WEBPACK_IMPORTED_MODULE_22__["HomedashComponent"],
                _web_webfooter_webfooter_component__WEBPACK_IMPORTED_MODULE_23__["WebfooterComponent"],
                _web_weblogin_weblogin_component__WEBPACK_IMPORTED_MODULE_24__["WebloginComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__["SocialLoginModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_16__["ToastrModule"].forRoot() // ToastrModule added
            ],
            providers: [_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"], _auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"], _guards_role_guard__WEBPACK_IMPORTED_MODULE_13__["RoleGuard"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HTTP_INTERCEPTORS"],
                    useClass: _token_interceptor_service__WEBPACK_IMPORTED_MODULE_15__["TokenInterceptorService"],
                    multi: true
                },
                {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_21__["AuthServiceConfig"],
                    useFactory: getAuthServiceConfigs
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(_authService, _router) {
        this._authService = _authService;
        this._router = _router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this._authService.loggedIn()) {
            console.log('true');
            return true;
        }
        else {
            console.log('false');
            this._router.navigate(['/admin']);
            return false;
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AuthService = /** @class */ (function () {
    function AuthService(_http, _router) {
        this._http = _http;
        this._router = _router;
    }
    AuthService.prototype.loginUser = function (userData) {
        var result = JSON.stringify(userData);
        return this._http.post('http://23.100.25.106:3300/users/login', JSON.parse(result));
    };
    AuthService.prototype.registerUser = function (userData) {
        var result = JSON.stringify(userData);
        return this._http.post('http://23.100.25.106:3300/users/signup', JSON.parse(result));
    };
    AuthService.prototype.decode = function () {
        var type = localStorage.getItem('type');
        return type;
    };
    AuthService.prototype.logoutUser = function () {
        localStorage.removeItem('token');
        this._router.navigate(['/admin']);
    };
    AuthService.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    AuthService.prototype.loggedIn = function () {
        return !!localStorage.getItem('token');
    };
    AuthService.prototype.forgotPassword = function (formData) {
        return this._http.post('http://23.100.25.106:3300/users/forgotPassword', formData);
    };
    AuthService.prototype.resetPassword = function (token, formData) {
        return this._http.post('http://23.100.25.106:3300/users/resetPassword/' + token, formData);
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pull-right\">\n  Innotech ©{{ copyYear }} All Rights Reserved.\n</div>\n<div class=\"clearfix\"></div>\n"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.copyYear = new Date().getFullYear();
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.css":
/*!***************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.html":
/*!****************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n      <div class=\"login_wrapper\">\n        <div class=\"animate form login_form\">\n          <section class=\"login_content\">\n            <form [formGroup]=\"forgotPasswordForm\" (ngSubmit)=\"onSubmit()\">\n              <h1>Forgot Password</h1>\n              <div>\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter your recovery email\" [email]=\"true\" required=\"\" />\n                <div *ngIf=\"email.invalid && (email.dirty || email.touched)\" class=\"alert alert-danger\">Email is required</div>\n              </div>\n              <div>\n                <!-- <button type=\"submit\" class=\"btn btn-default submit\">Log in</button> -->\n                <button type=\"submit\" [disabled]=\"!forgotPasswordForm.valid\" class=\"btn btn-default submit\">Submit</button>\n                <a class=\"reset_pass\" routerLink=\"/admin\">Login your Account?</a>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n              <div class=\"separator\">\n\n                <div class=\"clearfix\"></div>\n                <br />\n\n                <div>\n                  <h1><i class=\"fa fa-paw\"></i> Innotech</h1>\n                  <p>©2016 All Rights Reserved.</p>\n                </div>\n              </div>\n            </form>\n          </section>\n        </div>\n\n\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.ts ***!
  \**************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");







var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(fb, _auth, _router, toastr) {
        this.fb = fb;
        this._auth = _auth;
        this._router = _router;
        this.toastr = toastr;
        this.serverError = '';
        this.forgotPasswordForm = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
    };
    ForgotPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.forgotPasswordForm.value);
        this._auth.forgotPassword(this.forgotPasswordForm.value)
            .subscribe(function (result) {
            _this.toastr.success('Recovery mail send to you Successfully', 'Success!');
            _this._router.navigate(['/login']);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                    _this.toastr.error(_this.serverError, 'Major Error', { timeOut: 3000 });
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                    _this.toastr.error(_this.serverError, 'Major Error', { timeOut: 3000 });
                }
            }
        });
    };
    Object.defineProperty(ForgotPasswordComponent.prototype, "email", {
        get: function () { return this.forgotPasswordForm.get('email'); },
        enumerable: true,
        configurable: true
    });
    ForgotPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/forgot-password/forgot-password.component.html"),
            styles: [__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/forgot-password/forgot-password.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/guards/role.guard.ts":
/*!**************************************!*\
  !*** ./src/app/guards/role.guard.ts ***!
  \**************************************/
/*! exports provided: RoleGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGuard", function() { return RoleGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var RoleGuard = /** @class */ (function () {
    function RoleGuard(_authService, _router) {
        this._authService = _authService;
        this._router = _router;
    }
    RoleGuard.prototype.canActivate = function (next, state) {
        var user = this._authService.decode();
        if (user === next.data.role) {
            return true;
        }
        // navigate to not found page
        this._router.navigate(['/']);
        return false;
    };
    RoleGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], RoleGuard);
    return RoleGuard;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- menu profile quick info -->\n<div class=\"profile clearfix\">\n  <div class=\"profile_pic\">\n    <img src=\"{{ profileImageName }}\" alt=\"...\" class=\"img-circle profile_img\">\n  </div>\n  <div class=\"profile_info\">\n    <span>Welcome,</span>\n    <h2>{{ name }}</h2>\n  </div>\n</div>\n<!-- /menu profile quick info -->\n\n<br />\n<div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">\n  <div class=\"menu_section\">\n    <h3>General</h3>\n    <ul class=\"nav side-menu\" data-widget=tree>\n      <li><a><i class=\"fa fa-home\"></i> Home <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/dashboard\">Dashboard</a></li>\n        </ul>\n      </li>\n       <li><a><i class=\"fa fa-language\"></i> Users <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/user/list\">User List</a></li>\n        </ul>\n      </li>\n      <!-- <li><a><i class=\"fa fa-graduation-cap\"></i> Student <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/admission/list\">Student List</a></li>\n          <li><a routerLink=\"/admin/admission/add\">Add New Student</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-object-group\"></i> Branch <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/branch/list\">Branch List</a></li>\n          <li><a routerLink=\"/admin/branch/add\">Add New Branch</a></li>\n        </ul>\n      </li> -->\n      <li><a><i class=\"fa fa-language\"></i> Job <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/cms/list\">Job List</a></li>\n          <li><a routerLink=\"/admin/cms/add\">Add New Job</a></li>\n        </ul>\n      </li>\n   <!--    <li><a><i class=\"fa fa-money\"></i> Currency <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/currency/list\">Currency List</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-book\"></i> Contacts <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/contacts/list\">Contacts List</a></li>\n          <li><a routerLink=\"/admin/contacts/add\">Add Contacts</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-edit\"></i> Dummy list <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a href=\"form.html\">General Form</a></li>\n          <li><a href=\"form_advanced.html\">Advanced Components</a></li>\n          <li><a href=\"form_validation.html\">Form Validation</a></li>\n          <li><a href=\"form_wizards.html\">Form Wizard</a></li>\n          <li><a href=\"form_upload.html\">Form Upload</a></li>\n          <li><a href=\"form_buttons.html\">Form Buttons</a></li>\n        </ul>\n      </li> -->\n    </ul>\n  </div>\n\n\n</div>\n<!-- /sidebar menu -->\n\n<!-- /menu footer buttons -->\n<div class=\"sidebar-footer hidden-small\">\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">\n    <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">\n    <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">\n    <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" (click)=\"_authService.logoutUser()\">\n    <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>\n  </a>\n</div>\n<script>\n    $(document).ready(() => {\n      const trees: any = $('[data-widget=\"tree\"]');\n      trees.tree();\n    });\n    </script>\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(_authService) {
        this._authService = _authService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/homedash/homedash.component.css":
/*!*************************************************!*\
  !*** ./src/app/homedash/homedash.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVkYXNoL2hvbWVkYXNoLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/homedash/homedash.component.html":
/*!**************************************************!*\
  !*** ./src/app/homedash/homedash.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  homedash works!\n</p>\n"

/***/ }),

/***/ "./src/app/homedash/homedash.component.ts":
/*!************************************************!*\
  !*** ./src/app/homedash/homedash.component.ts ***!
  \************************************************/
/*! exports provided: HomedashComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomedashComponent", function() { return HomedashComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomedashComponent = /** @class */ (function () {
    function HomedashComponent() {
    }
    HomedashComponent.prototype.ngOnInit = function () {
    };
    HomedashComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-homedash',
            template: __webpack_require__(/*! ./homedash.component.html */ "./src/app/homedash/homedash.component.html"),
            styles: [__webpack_require__(/*! ./homedash.component.css */ "./src/app/homedash/homedash.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomedashComponent);
    return HomedashComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n      <div class=\"login_wrapper\">\n        <div class=\"animate form login_form\">\n          <section class=\"login_content\">\n            <form [formGroup]=\"loginForm\" (ngSubmit)=\"loginUser()\">\n              <h1>Login Form</h1>\n              <div>\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Username\" required=\"\" />\n              </div>\n              <div>\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Password\" required=\"\" />\n              </div>\n\n              <input type=\"hidden\" formControlName=\"type\" value=\"2\" />\n              \n              <div>\n                <!-- <button type=\"submit\" class=\"btn btn-default submit\">Log in</button> -->\n                <button type=\"submit\" [disabled]=\"!loginForm.valid\" class=\"btn btn-default submit\">Log in</button>\n                <a class=\"reset_pass\" routerLink=\"/forgot-password\">Lost your password?</a>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n              <div class=\"separator\">\n                \n\n                <div class=\"clearfix\"></div>\n                <br />\n\n                <div>\n                  <h1> <img src=\"./assets/images/innologo.png\" width=\"200px\" height=\"100px\"></h1>\n                  <p>©{{ copyYear }} All Rights Reserved.</p>\n                </div>\n              </div>\n            </form>\n          </section>\n        </div>\n\n\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





var LoginComponent = /** @class */ (function () {
    function LoginComponent(_auth, _router) {
        this._auth = _auth;
        this._router = _router;
        this.loginUserData = {};
        this.copyYear = new Date().getFullYear();
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm.setValue({ "type": 1, "password": "", "email": "" });
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        this._auth.loginUser(this.loginForm.value)
            .subscribe(function (res) {
            localStorage.setItem('token', res.token);
            localStorage.setItem('name', res.data.name);
            localStorage.setItem('profileImageName', res.data.profileImageName);
            _this._router.navigate(['/admin/dashboard']);
        }, function (err) { return console.log(err); });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/logout/logout.component.css":
/*!*********************************************!*\
  !*** ./src/app/logout/logout.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/logout/logout.component.html":
/*!**********************************************!*\
  !*** ./src/app/logout/logout.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  logout works!\n</p>\n"

/***/ }),

/***/ "./src/app/logout/logout.component.ts":
/*!********************************************!*\
  !*** ./src/app/logout/logout.component.ts ***!
  \********************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





var LogoutComponent = /** @class */ (function () {
    function LogoutComponent(auth, user, router) {
        this.auth = auth;
        this.user = user;
        this.router = router;
    }
    LogoutComponent.prototype.ngOnInit = function () {
    };
    LogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-logout',
            template: __webpack_require__(/*! ./logout.component.html */ "./src/app/logout/logout.component.html"),
            styles: [__webpack_require__(/*! ./logout.component.css */ "./src/app/logout/logout.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.css":
/*!*************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.html":
/*!**************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  page-not-found works!\n</p>\n"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.ts":
/*!************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.ts ***!
  \************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/page-not-found/page-not-found.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.css":
/*!*************************************************!*\
  !*** ./src/app/register/register.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div>\n      <div class=\"login_wrapper\">\n        <div class=\"animate form login_form\">\n          <section class=\"login_content\">\n            <form [formGroup]=\"registerForm\" (ngSubmit)=\"registerUser()\">\n              <h1>Register Form</h1>\n              <div  *ngIf=\"msg != '' \" style=\"color:red; text-align:center\"> {{ msg }} </div>\n              <div class=\"form-group\">\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email\" required=\"\" />\n\n                <span *ngIf=\"email.invalid && (email.dirty || email.touched)\" style=\"color:red; float:left;margin-top: -13px;padding-bottom: 8px;\">You must enter a valid email</span>\n\n              </div >\n              <div class=\"form-group\">\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Password\" required=\"\" />\n\n                 <span *ngIf=\"password.invalid && (password.dirty || password.touched)\" style=\"color:red; float:left;margin-top: -13px;padding-bottom: 8px;\">Password is required</span>\n\n              </div>\n              <div>\n\n                <input type=\"hidden\" formControlName=\"type\" />\n\n                <!-- <button type=\"submit\" class=\"btn btn-default submit\">Log in</button> -->\n                <button type=\"submit\" [disabled]=\"!registerForm.valid\" class=\"btn btn-default submit\">Sign Up</button>\n                <a class=\"reset_pass\" routerLink=\"/forgot-password\">Lost your password?</a>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n              <div class=\"separator\">\n                <p class=\"change_link\">Already Register?\n                  <a routerLink=\"/\" class=\"to_register\"> Login your Account </a>\n                </p>\n\n                <div class=\"clearfix\"></div>\n                <br />\n\n                <div>\n                  <h1><img src=\"./assets/images/innologo.png\" width=\"200px\" height=\"100px\"></h1>\n                  <p>©{{ copyYear }} All Rights Reserved.</p>\n                </div>\n              </div>\n            </form>\n          </section>\n        </div>\n\n\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_auth, _fb, _router) {
        this._auth = _auth;
        this._fb = _fb;
        this._router = _router;
        this.copyYear = new Date().getFullYear();
        this.msg = '';
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = this._fb.group({
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(new RegExp(/^[a-z0-9][a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/))]],
            password: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            type: ["", ""]
        });
        this.registerForm.setValue({ "type": 1, "password": "", "email": "" });
    };
    Object.defineProperty(RegisterComponent.prototype, "email", {
        /*getter for email*/
        get: function () {
            return this.registerForm.get("email");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "password", {
        /*getter for password*/
        get: function () {
            return this.registerForm.get("password");
        },
        enumerable: true,
        configurable: true
    });
    RegisterComponent.prototype.registerUser = function () {
        var _this = this;
        this.msg = '';
        this._auth.registerUser(this.registerForm.value)
            .subscribe(function (res) {
            localStorage.setItem('token', res.token);
            _this._router.navigate(['/admin/dashboard']);
        }, function (err) {
            _this.msg = err.error.message;
            console.log(err);
        });
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/reset-password/password-validation.ts":
/*!*******************************************************!*\
  !*** ./src/app/reset-password/password-validation.ts ***!
  \*******************************************************/
/*! exports provided: PasswordValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidation", function() { return PasswordValidation; });
var PasswordValidation = /** @class */ (function () {
    function PasswordValidation() {
    }
    PasswordValidation.MatchPassword = function (AC) {
        var password = AC.get('password').value; // to get value in input tag
        var confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if (password != confirmPassword) {
            console.log('false');
            AC.get('confirmPassword').setErrors({ MatchPassword: true });
        }
        else {
            console.log('true');
            return null;
        }
    };
    return PasswordValidation;
}());



/***/ }),

/***/ "./src/app/reset-password/reset-password.component.css":
/*!*************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc2V0LXBhc3N3b3JkL3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/reset-password/reset-password.component.html":
/*!**************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n      <div class=\"login_wrapper\">\n        <div class=\"animate form login_form\">\n          <section class=\"login_content\">\n            <form [formGroup]=\"resetPasswordForm\" (ngSubmit)=\"onSubmit()\">\n              <h1>Reset Password</h1>\n              <div>\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"email\" placeholder=\"Enter new password\" required=\"\" />\n                <div *ngIf=\"password.invalid && (password.dirty || password.touched)\" class=\"alert alert-danger\">New Password is requird</div>\n              </div>\n              <div>\n                <input type=\"password\" formControlName=\"confirmPassword\" class=\"form-control\" placeholder=\"Confirm Password\" required >\n                <div *ngIf=\"confirmPassword.invalid && (confirmPassword.dirty || confirmPassword.touched)\" class=\"alert alert-danger\">Confirm Password is requird and must be equal</div>\n              </div>\n              <div>\n                <!-- <button type=\"submit\" class=\"btn btn-default submit\">Log in</button> -->\n                <button type=\"submit\" [disabled]=\"!resetPasswordForm.valid\" class=\"btn btn-default submit\">Submit</button>\n                <a class=\"reset_pass\" routerLink=\"/login\">Login your Account?</a>\n              </div>\n\n              <div class=\"clearfix\"></div>\n\n              <div class=\"separator\">\n                <p class=\"change_link\">New to site?\n                  <a routerLink=\"/register\" class=\"to_register\"> Create Account </a>\n                </p>\n\n                <div class=\"clearfix\"></div>\n                <br />\n\n                <div>\n                  <h1><i class=\"fa fa-paw\"></i> Innotech</h1>\n                  <p>©2016 All Rights Reserved.</p>\n                </div>\n              </div>\n            </form>\n          </section>\n        </div>\n\n\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/reset-password/reset-password.component.ts":
/*!************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.ts ***!
  \************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _password_validation__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./password-validation */ "./src/app/reset-password/password-validation.ts");








var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(fb, _auth, _router, toastr, activatedRoute) {
        this.fb = fb;
        this._auth = _auth;
        this._router = _router;
        this.toastr = toastr;
        this.activatedRoute = activatedRoute;
        this.serverError = '';
        this.resetPasswordForm = this.fb.group({
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        }, { validator: _password_validation__WEBPACK_IMPORTED_MODULE_7__["PasswordValidation"].MatchPassword });
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(ResetPasswordComponent.prototype, "password", {
        get: function () { return this.resetPasswordForm.get('password'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ResetPasswordComponent.prototype, "confirmPassword", {
        get: function () { return this.resetPasswordForm.get('confirmPassword'); },
        enumerable: true,
        configurable: true
    });
    ResetPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        this.token = this.activatedRoute.snapshot.paramMap.get('token');
        this._auth.resetPassword(this.token, this.resetPasswordForm.value)
            .subscribe(function (result) {
            _this.toastr.success('Password changed Successfully', 'Success!');
            _this._router.navigate(['/login']);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                    _this.toastr.error(_this.serverError, 'Major Error', { timeOut: 3000 });
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                    _this.toastr.error(_this.serverError, 'Major Error', { timeOut: 3000 });
                }
            }
        });
    };
    ResetPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__(/*! ./reset-password.component.html */ "./src/app/reset-password/reset-password.component.html"),
            styles: [__webpack_require__(/*! ./reset-password.component.css */ "./src/app/reset-password/reset-password.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/sidenav/sidenav.component.css":
/*!***********************************************!*\
  !*** ./src/app/sidenav/sidenav.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZGVuYXYvc2lkZW5hdi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/sidenav/sidenav.component.html":
/*!************************************************!*\
  !*** ./src/app/sidenav/sidenav.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- menu profile quick info -->\n<div class=\"profile clearfix\">\n  <div class=\"profile_pic\">\n  </div>\n  <div class=\"profile_info\">\n    <span>Welcome,</span>\n    <h2>{{ name }}</h2>\n  </div>\n</div>\n<!-- /menu profile quick info -->\n\n<br />\n<div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">\n  <div class=\"menu_section\">\n    <h3>General</h3>\n    <ul class=\"nav side-menu\" data-widget=tree>\n      <li><a><i class=\"fa fa-home\"></i> Home <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/dashboard\">Dashboard</a></li>\n        </ul>\n      </li>\n       <li><a><i class=\"fa fa-language\"></i> Users <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/user/list\">User List</a></li>\n        </ul>\n      </li>\n      <!-- <li><a><i class=\"fa fa-graduation-cap\"></i> Student <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/admission/list\">Student List</a></li>\n          <li><a routerLink=\"/admin/admission/add\">Add New Student</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-object-group\"></i> Branch <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/branch/list\">Branch List</a></li>\n          <li><a routerLink=\"/admin/branch/add\">Add New Branch</a></li>\n        </ul>\n      </li> -->\n      <li><a><i class=\"fa fa-language\"></i> Job <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/job/list\">Job List</a></li>\n          <li><a routerLink=\"/admin/job/add\">Add New Job</a></li>\n        </ul>\n      </li>\n   <!--    <li><a><i class=\"fa fa-money\"></i> Currency <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/currency/list\">Currency List</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-book\"></i> Contacts <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a routerLink=\"/admin/contacts/list\">Contacts List</a></li>\n          <li><a routerLink=\"/admin/contacts/add\">Add Contacts</a></li>\n        </ul>\n      </li>\n      <li><a><i class=\"fa fa-edit\"></i> Dummy list <span class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li><a href=\"form.html\">General Form</a></li>\n          <li><a href=\"form_advanced.html\">Advanced Components</a></li>\n          <li><a href=\"form_validation.html\">Form Validation</a></li>\n          <li><a href=\"form_wizards.html\">Form Wizard</a></li>\n          <li><a href=\"form_upload.html\">Form Upload</a></li>\n          <li><a href=\"form_buttons.html\">Form Buttons</a></li>\n        </ul>\n      </li> -->\n    </ul>\n  </div>\n\n\n</div>\n<!-- /sidebar menu -->\n\n<!-- /menu footer buttons -->\n<div class=\"sidebar-footer hidden-small\">\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">\n    <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">\n    <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">\n    <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>\n  </a>\n  <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" (click)=\"_authService.logoutUser()\">\n    <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>\n  </a>\n</div>\n<script>\n    $(document).ready(() => {\n      const trees: any = $('[data-widget=\"tree\"]');\n      trees.tree();\n    });\n    </script>\n"

/***/ }),

/***/ "./src/app/sidenav/sidenav.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidenav/sidenav.component.ts ***!
  \**********************************************/
/*! exports provided: SidenavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidenavComponent", function() { return SidenavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");




var SidenavComponent = /** @class */ (function () {
    function SidenavComponent(_authService) {
        this._authService = _authService;
        this.name = localStorage.getItem('name');
        this.profileImageName = localStorage.getItem('profileImageName');
    }
    SidenavComponent.prototype.ngDoCheck = function () {
        var minHeight = jquery__WEBPACK_IMPORTED_MODULE_2__(document).height();
        var maxHeight = jquery__WEBPACK_IMPORTED_MODULE_2__(document).height();
        jquery__WEBPACK_IMPORTED_MODULE_2__('.left_col').css("min-height", minHeight);
        //  $('.left_col').css("max-height", maxHeight);
    };
    SidenavComponent.prototype.ngOnInit = function () {
        var CURRENT_URL = window.location.href.split('#')[0].split('?')[0], $BODY = jquery__WEBPACK_IMPORTED_MODULE_2__('body'), $MENU_TOGGLE = jquery__WEBPACK_IMPORTED_MODULE_2__('#menu_toggle'), $SIDEBAR_MENU = jquery__WEBPACK_IMPORTED_MODULE_2__('#sidebar-menu'), $SIDEBAR_FOOTER = jquery__WEBPACK_IMPORTED_MODULE_2__('.sidebar-footer'), $LEFT_COL = jquery__WEBPACK_IMPORTED_MODULE_2__('.left_col'), $RIGHT_COL = jquery__WEBPACK_IMPORTED_MODULE_2__('.right_col'), $NAV_MENU = jquery__WEBPACK_IMPORTED_MODULE_2__('.nav_menu'), $FOOTER = jquery__WEBPACK_IMPORTED_MODULE_2__('footer');
        // Sidebar
        jquery__WEBPACK_IMPORTED_MODULE_2__(document).ready(function () {
            jquery__WEBPACK_IMPORTED_MODULE_2__("body").addClass("nav-md");
            // TODO: This is some kind of easy fix, maybe we can improve this
            var setContentHeight = function () {
                // reset height
                $RIGHT_COL.css('min-height', jquery__WEBPACK_IMPORTED_MODULE_2__(window).height());
                var bodyHeight = $BODY.outerHeight(), footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(), leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(), contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
                // normalize content
                contentHeight -= $NAV_MENU.height() + footerHeight;
                $RIGHT_COL.css('min-height', contentHeight);
            };
            $SIDEBAR_MENU.find('a').on('click', function (ev) {
                var $li = jquery__WEBPACK_IMPORTED_MODULE_2__(this).parent();
                if ($li.is('.active')) {
                    $li.removeClass('active active-sm');
                    jquery__WEBPACK_IMPORTED_MODULE_2__('ul:first', $li).slideUp(function () {
                        setContentHeight();
                    });
                }
                else {
                    // prevent closing menu if we are on child menu
                    if (!$li.parent().is('.child_menu')) {
                        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        $SIDEBAR_MENU.find('li ul').slideUp();
                    }
                    $li.addClass('active');
                    jquery__WEBPACK_IMPORTED_MODULE_2__('ul:first', $li).slideDown(function () {
                        setContentHeight();
                    });
                }
            });
            // toggle small or large menu
            $MENU_TOGGLE.on('click', function () {
                if ($BODY.hasClass('nav-md')) {
                    $SIDEBAR_MENU.find('li.active ul').hide();
                    $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                }
                else {
                    $SIDEBAR_MENU.find('li.active-sm ul').show();
                    $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                }
                $BODY.toggleClass('nav-md nav-sm');
                setContentHeight();
                jquery__WEBPACK_IMPORTED_MODULE_2__('.dataTable').each(function () { jquery__WEBPACK_IMPORTED_MODULE_2__(this).dataTable().fnDraw(); });
            });
            // check active menu
            $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');
            $SIDEBAR_MENU.find('a').filter(function () {
                return this.href == CURRENT_URL;
            }).parent('li').addClass('current-page').parents('ul').slideDown(function () {
                setContentHeight();
            }).parent().addClass('active');
            setContentHeight();
            // fixed sidebar
            if (jquery__WEBPACK_IMPORTED_MODULE_2__["fn"].mCustomScrollbar) {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.menu_fixed').mCustomScrollbar({
                    autoHideScrollbar: true,
                    theme: 'minimal',
                    mouseWheel: { preventDefault: true }
                });
            }
        });
        // /Sidebar
    };
    SidenavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidenav',
            template: __webpack_require__(/*! ./sidenav.component.html */ "./src/app/sidenav/sidenav.component.html"),
            styles: [__webpack_require__(/*! ./sidenav.component.css */ "./src/app/sidenav/sidenav.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], SidenavComponent);
    return SidenavComponent;
}());



/***/ }),

/***/ "./src/app/token-interceptor.service.ts":
/*!**********************************************!*\
  !*** ./src/app/token-interceptor.service.ts ***!
  \**********************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");



var TokenInterceptorService = /** @class */ (function () {
    function TokenInterceptorService(injector, _authService) {
        this.injector = injector;
        this._authService = _authService;
    }
    TokenInterceptorService.prototype.intercept = function (req, next) {
        //let authService = this.injector.get(AuthService)
        var tokenizedReq = req.clone({
            headers: req.headers.set('Authorization', 'bearer ' + this._authService.getToken())
        });
        return next.handle(tokenizedReq);
    };
    TokenInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"], _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], TokenInterceptorService);
    return TokenInterceptorService;
}());



/***/ }),

/***/ "./src/app/topnav/topnav.component.css":
/*!*********************************************!*\
  !*** ./src/app/topnav/topnav.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RvcG5hdi90b3BuYXYuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/topnav/topnav.component.html":
/*!**********************************************!*\
  !*** ./src/app/topnav/topnav.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- top navigation -->\n<div class=\"top_nav\">\n  <div class=\"nav_menu\">\n    <nav>\n      <div class=\"nav toggle\">\n        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>\n      </div>\n\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li class=\"\">\n          <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n            <img src=\"{{ profileImageName }}\" alt=\"\">{{ name }}\n            <span class=\" fa fa-angle-down\"></span>\n          </a>\n          <ul class=\"dropdown-menu dropdown-usermenu pull-right\">\n            <li><!--<a href=\"javascript:;\"> Profile</a>-->\n              <a [routerLink]=\"['/admin/profile']\"> Profile</a>\n            </li>\n            <li>\n              <a [routerLink]=\"['/admin/change-password']\">\n                Change Password\n              </a>\n            </li>\n            <li><a href=\"javascript:;\">Help</a></li>\n            <li><a (click)=\"_authService.logoutUser()\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>\n          </ul>\n        </li>\n\n        <li role=\"presentation\" class=\"dropdown\">\n          <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n            <i class=\"fa fa-envelope-o\"></i>\n            <span class=\"badge bg-green\">6</span>\n          </a>\n          <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list\" role=\"menu\">\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <a>\n                <span class=\"image\"><img src=\"assets/images/img.jpg\" alt=\"Profile Image\" /></span>\n                <span>\n                  <span>John Smith</span>\n                  <span class=\"time\">3 mins ago</span>\n                </span>\n                <span class=\"message\">\n                  Film festivals used to be do-or-die moments for movie makers. They were where...\n                </span>\n              </a>\n            </li>\n            <li>\n              <div class=\"text-center\">\n                <a>\n                  <strong>See All Alerts</strong>\n                  <i class=\"fa fa-angle-right\"></i>\n                </a>\n              </div>\n            </li>\n          </ul>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/topnav/topnav.component.ts":
/*!********************************************!*\
  !*** ./src/app/topnav/topnav.component.ts ***!
  \********************************************/
/*! exports provided: TopnavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopnavComponent", function() { return TopnavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");



var TopnavComponent = /** @class */ (function () {
    function TopnavComponent(_authService) {
        this._authService = _authService;
        this.name = localStorage.getItem('name');
        this.profileImageName = localStorage.getItem('profileImageName');
    }
    TopnavComponent.prototype.ngOnInit = function () {
    };
    TopnavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-topnav',
            template: __webpack_require__(/*! ./topnav.component.html */ "./src/app/topnav/topnav.component.html"),
            styles: [__webpack_require__(/*! ./topnav.component.css */ "./src/app/topnav/topnav.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], TopnavComponent);
    return TopnavComponent;
}());



/***/ }),

/***/ "./src/app/user.service.ts":
/*!*********************************!*\
  !*** ./src/app/user.service.ts ***!
  \*********************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserService = /** @class */ (function () {
    function UserService() {
    }
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/web/webfooter/webfooter.component.css":
/*!*******************************************************!*\
  !*** ./src/app/web/webfooter/webfooter.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi93ZWJmb290ZXIvd2ViZm9vdGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/web/webfooter/webfooter.component.html":
/*!********************************************************!*\
  !*** ./src/app/web/webfooter/webfooter.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"pull-right\">\n  Innotech ©2016 All Rights Reserved.\n</div>\n<div class=\"clearfix\"></div>\n"

/***/ }),

/***/ "./src/app/web/webfooter/webfooter.component.ts":
/*!******************************************************!*\
  !*** ./src/app/web/webfooter/webfooter.component.ts ***!
  \******************************************************/
/*! exports provided: WebfooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebfooterComponent", function() { return WebfooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WebfooterComponent = /** @class */ (function () {
    function WebfooterComponent() {
    }
    WebfooterComponent.prototype.ngOnInit = function () {
    };
    WebfooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-webfooter',
            template: __webpack_require__(/*! ./webfooter.component.html */ "./src/app/web/webfooter/webfooter.component.html"),
            styles: [__webpack_require__(/*! ./webfooter.component.css */ "./src/app/web/webfooter/webfooter.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WebfooterComponent);
    return WebfooterComponent;
}());



/***/ }),

/***/ "./src/app/web/weblogin/weblogin.component.css":
/*!*****************************************************!*\
  !*** ./src/app/web/weblogin/weblogin.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* login section css start here */\n#loginid .panel-default>.panel-heading {\n    color:white;\n    background-color: #337ab7;\n    border-color: #ddd;\n}\n#loginid{\n    min-height: 625px;\n    padding:70px;\nbackground: linear-gradient(to right, #e9e4f012, #d3cce32e);\n}\n#login-box{\n    max-width: 500px;\n    min-width: 250px;\n    overflow: hidden;\n    margin: 0 auto;\n}\n#loginid h2 {\n     color: white;\n    text-align: center;\n    margin: 0px;\n    font-size:20px;\n    font-family: serif;\n}\n.bottom{\n    background-color:rgba(255,255,255,.8);\n    border-top:1px solid #ddd;\n    clear:both;\n    padding:14px;\n}\n.social-buttons{\n    margin:12px 0    \n}\n.social-buttons a{\n    width: 49%;\n}\n.form-group {\n    margin-bottom: 10px;\n}\n.btn-fb{\n    color: #fff;\n    background-color:#3b5998;\n}\n.btn-fb:hover{\n    color: #fff;\n    background-color:#496ebc \n}\n.btn-tw{\n    color: #fff;\n    background-color:#55acee;\n}\n.btn-tw:hover{\n    color: #fff;\n    background-color:#59b5fa;\n}\n/* login section css start here */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL3dlYmxvZ2luL3dlYmxvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsaUNBQWlDO0FBQ2pDO0lBQ0ksV0FBVztJQUNYLHlCQUF5QjtJQUN6QixrQkFBa0I7QUFDdEI7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixZQUFZO0FBR2hCLDJEQUEyRDtBQUMzRDtBQUdBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjtBQUdBO0tBQ0ssWUFBWTtJQUNiLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsY0FBYztJQUNkLGtCQUFrQjtBQUN0QjtBQUdDO0lBQ0cscUNBQXFDO0lBQ3JDLHlCQUF5QjtJQUN6QixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNDO0lBQ0c7QUFDSjtBQUNDO0lBQ0csVUFBVTtBQUNkO0FBQ0M7SUFDRyxtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLFdBQVc7SUFDWCx3QkFBd0I7QUFDNUI7QUFDQTtJQUNJLFdBQVc7SUFDWDtBQUNKO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBRUEsaUNBQWlDIiwiZmlsZSI6InNyYy9hcHAvd2ViL3dlYmxvZ2luL3dlYmxvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBsb2dpbiBzZWN0aW9uIGNzcyBzdGFydCBoZXJlICovXG4jbG9naW5pZCAucGFuZWwtZGVmYXVsdD4ucGFuZWwtaGVhZGluZyB7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzMzN2FiNztcbiAgICBib3JkZXItY29sb3I6ICNkZGQ7XG59XG5cbiNsb2dpbmlke1xuICAgIG1pbi1oZWlnaHQ6IDYyNXB4O1xuICAgIHBhZGRpbmc6NzBweDtcbmJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZTllNGYwMTIsICNkM2NjZTMyZSk7XG5iYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2U5ZTRmMDEyLCAjZDNjY2UzMmUpO1xuYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZTllNGYwMTIsICNkM2NjZTMyZSk7XG59XG5cblxuI2xvZ2luLWJveHtcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICAgIG1pbi13aWR0aDogMjUwcHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBtYXJnaW46IDAgYXV0bztcbn1cblxuXG4jbG9naW5pZCBoMiB7XG4gICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgZm9udC1zaXplOjIwcHg7XG4gICAgZm9udC1mYW1pbHk6IHNlcmlmO1xufVxuXG5cbiAuYm90dG9te1xuICAgIGJhY2tncm91bmQtY29sb3I6cmdiYSgyNTUsMjU1LDI1NSwuOCk7XG4gICAgYm9yZGVyLXRvcDoxcHggc29saWQgI2RkZDtcbiAgICBjbGVhcjpib3RoO1xuICAgIHBhZGRpbmc6MTRweDtcbn1cbiAuc29jaWFsLWJ1dHRvbnN7XG4gICAgbWFyZ2luOjEycHggMCAgICBcbn1cbiAuc29jaWFsLWJ1dHRvbnMgYXtcbiAgICB3aWR0aDogNDklO1xufVxuIC5mb3JtLWdyb3VwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuLmJ0bi1mYntcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMzYjU5OTg7XG59XG4uYnRuLWZiOmhvdmVye1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQtY29sb3I6IzQ5NmViYyBcbn1cbi5idG4tdHd7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojNTVhY2VlO1xufVxuLmJ0bi10dzpob3ZlcntcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiM1OWI1ZmE7XG59XG5cbi8qIGxvZ2luIHNlY3Rpb24gY3NzIHN0YXJ0IGhlcmUgKi8iXX0= */"

/***/ }),

/***/ "./src/app/web/weblogin/weblogin.component.html":
/*!******************************************************!*\
  !*** ./src/app/web/weblogin/weblogin.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"loginid\">\n    <div class=\"container\">\n  \t <div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t    <div id=\"login-box\">\n\t\t\t\t\t\t\t    <div class=\"panel panel-default\">\n\t\t\t\t\t\t\t        <div class=\"panel-heading\">\n\t\t\t\t\t\t\t            <h2>Login Here</h2>\n\t\t\t\t\t\t\t        </div>\n\t\t\t\t\t\t\t        <div class=\"panel-body\">\n\t\t\t\t\t\t\t\tLogin via\n\t\t\t\t\t\t\t\t<div class=\"social-buttons\">\n\t\t\t\t\t\t\t\t\t<button type=\"button\" (click)=\"socialSignIn()\" class=\"btn btn-fb\"><i class=\"fa fa-facebook\"></i> Facebook</button>\n\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-tw\"><i class=\"fa fa-twitter\"></i> Twitter</a>\n\t\t\t\t\t\t\t\t</div>\n                               OR <br>\n\n                                 <div  *ngIf=\"msg != '' \" style=\"color:red; text-align:center\"> {{ msg }} </div>\n\n\t\t\t\t\t\t\t\t <form class=\"form\" [formGroup]=\"loginForm\" (ngSubmit)=\"loginUser()\" method=\"post\" action=\"login\" accept-charset=\"UTF-8\" id=\"login-nav\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t <label class=\"sr-only\" for=\"exampleInputEmail2\">Email address</label>\n\n\t\t\t\t\t\t\t\t\t\t\t <input type=\"text\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail2\" placeholder=\"Email address\" required=\"\" />\n\n\t\t\t\t\t\t\t\t\t\t\t <span *ngIf=\"email.invalid && (email.dirty || email.touched)\" style=\"color:red\">You must enter a valid email</span>\n\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" formControlName=\"type\" value=\"2\" />\n\n\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t <label class=\"sr-only\" for=\"exampleInputPassword2\">Password</label>\n\t\t\t\t\t\t\t\t\t\t\t \n\t\t\t\t\t\t\t\t\t\t\t <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"exampleInputPassword2\" placeholder=\"Password\" required=\"\" />\n\n\t\t\t\t\t\t\t\t\t\t\t <span *ngIf=\"password.invalid && (password.dirty || password.touched)\" style=\"color:red\">Password is required</span>\n\n                                             <div class=\"help-block text-right\"><a href=\"\">Forget the password ?</a></div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t                                      <div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" [disabled]=\"loginForm.invalid\" class=\"btn btn-primary btn-block\">Sign in</button>\n\t                                      </div>\n\n\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox\">\n\t\t\t\t\t\t\t\t\t\t\t <label>\n\t\t\t\t\t\t\t\t\t\t\t <input type=\"checkbox\"> keep me logged-in\n\t\t\t\t\t\t\t\t\t\t\t </label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t </form>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"bottom text-center\">\n\t\t\t\t\t\t\t\t<p class=\"change_link\">New to site?\n\t\t\t\t                  <a routerLink=\"/register\" class=\"to_register\"> Create Account </a>\n\t\t\t\t                </p>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"bottom text-center\">\n\t\t\t\t\t\t\t\t<p class=\"change_link\">Click here ?\n\t\t\t\t                  <a routerLink=\"/admin\" class=\"to_register\"> Admin login </a>\n\t\t\t\t                </p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n</div>\n</div>\n</div>\n</section>\n"

/***/ }),

/***/ "./src/app/web/weblogin/weblogin.component.ts":
/*!****************************************************!*\
  !*** ./src/app/web/weblogin/weblogin.component.ts ***!
  \****************************************************/
/*! exports provided: WebloginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebloginComponent", function() { return WebloginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__);






var WebloginComponent = /** @class */ (function () {
    function WebloginComponent(socialAuthService, _fb, _auth, _router) {
        this.socialAuthService = socialAuthService;
        this._fb = _fb;
        this._auth = _auth;
        this._router = _router;
        this.msg = '';
    }
    Object.defineProperty(WebloginComponent.prototype, "email", {
        /*getter for email*/
        get: function () {
            return this.loginForm.get("email");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebloginComponent.prototype, "password", {
        /*getter for password*/
        get: function () {
            return this.loginForm.get("password");
        },
        enumerable: true,
        configurable: true
    });
    WebloginComponent.prototype.ngOnInit = function () {
        this.loginForm = this._fb.group({
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(new RegExp(/^[a-z0-9][a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/))]],
            password: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            type: ["", ""]
        });
        this.loginForm.setValue({ "type": 2, "password": "", "email": "" });
    };
    WebloginComponent.prototype.socialSignIn = function () {
        var _this = this;
        var socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["FacebookLoginProvider"].PROVIDER_ID;
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            console.log("facebook" + " sign in data : ", userData);
            // Now sign-in with userData
            // ...
            _this.user = { "id": userData.id, "name": userData.name, "image": userData.image, "email": userData.email };
            localStorage.setItem('user', JSON.stringify(_this.user));
            localStorage.setItem('type', '1');
            _this._router.navigate(['/home']);
        }).catch(function (e) {
            console.log('errord', e); // 'worky!'
        });
    };
    WebloginComponent.prototype.loginUser = function () {
        var _this = this;
        this.msg = '';
        this._auth.loginUser(this.loginForm.value)
            .subscribe(function (res) {
            localStorage.setItem('token', res.token);
            localStorage.setItem('name', res.data.name);
            localStorage.setItem('type', res.data.type);
            localStorage.setItem('name', res.data.name);
            localStorage.setItem('profileImageName', res.data.profileImageName);
            _this._router.navigate(['/home']);
        }, function (err) {
            _this.msg = err.error.message;
            console.log("wer", err);
        });
    };
    WebloginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-weblogin',
            template: __webpack_require__(/*! ./weblogin.component.html */ "./src/app/web/weblogin/weblogin.component.html"),
            styles: [__webpack_require__(/*! ./weblogin.component.css */ "./src/app/web/weblogin/weblogin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [angular_6_social_login__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], WebloginComponent);
    return WebloginComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/innotech/admin/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map