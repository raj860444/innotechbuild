(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["currency-currency-module"],{

/***/ "./src/app/admin/currency/currency-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/admin/currency/currency-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CurrencyRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyRoutingModule", function() { return CurrencyRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/currency/list/list.component.ts");




var routes = [
    { path: '', component: _list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"] },
    { path: 'list', component: _list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"] }
];
var CurrencyRoutingModule = /** @class */ (function () {
    function CurrencyRoutingModule() {
    }
    CurrencyRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CurrencyRoutingModule);
    return CurrencyRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/currency/currency.module.ts":
/*!***************************************************!*\
  !*** ./src/app/admin/currency/currency.module.ts ***!
  \***************************************************/
/*! exports provided: CurrencyModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyModule", function() { return CurrencyModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _currency_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./currency-routing.module */ "./src/app/admin/currency/currency-routing.module.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/currency/list/list.component.ts");





var CurrencyModule = /** @class */ (function () {
    function CurrencyModule() {
    }
    CurrencyModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _currency_routing_module__WEBPACK_IMPORTED_MODULE_3__["CurrencyRoutingModule"]
            ]
        })
    ], CurrencyModule);
    return CurrencyModule;
}());



/***/ }),

/***/ "./src/app/admin/currency/list/list.component.css":
/*!********************************************************!*\
  !*** ./src/app/admin/currency/list/list.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2N1cnJlbmN5L2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/currency/list/list.component.html":
/*!*********************************************************!*\
  !*** ./src/app/admin/currency/list/list.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>All Branches</h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>All Branches <small>Custom design</small></h2>\n                        <div class=\"clearfix\"></div>\n                    </div>\n\n                    <div class=\"x_content\">\n                        <div class=\"table-responsive\">\n                          <table class=\"table table-striped jambo_table bulk_action\">\n                            <thead>\n                                <tr class=\"headings\">\n                                  <th>\n                                    <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                  </th>\n                                    <th class=\"column-title\">Invoice </th>\n                                    <th class=\"column-title\">Invoice Date </th>\n                                    <th class=\"column-title\">Order </th>\n                                    <th class=\"column-title\">Bill to Name </th>\n                                    <th class=\"column-title\">Status </th>\n                                    <th class=\"column-title\">Amount </th>\n                                    <th class=\"column-title no-link last\"><span class=\"nobr\">Action</span>\n                                    </th>\n                                    <th class=\"bulk-actions\" colspan=\"7\">\n                                        <a class=\"antoo\" style=\"color:#fff; font-weight:500;\">Bulk Actions ( <span class=\"action-cnt\"> </span> ) <i class=\"fa fa-chevron-down\"></i></a>\n                                    </th>\n                                </tr>\n                            </thead>\n\n                            <tbody>\n                                <tr class=\"even pointer\">\n                                    <td>\n                                      <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                    </td>\n                                    <td class=\" \">121000040</td>\n                                    <td class=\" \">May 23, 2014 11:47:56 PM </td>\n                                    <td class=\" \">121000210 <i class=\"success fa fa-long-arrow-up\"></i></td>\n                                    <td class=\" \">John Blank L</td>\n                                    <td class=\" \">Paid</td>\n                                    <td class=\"a-right a-right \">$7.45</td>\n                                    <td class=\" last\"><a href=\"#\">View</a>\n                                    </td>\n                                </tr>\n                                <tr class=\"odd pointer\">\n                                    <td>\n                                      <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                    </td>\n                                    <td class=\" \">121000039</td>\n                                    <td class=\" \">May 23, 2014 11:30:12 PM</td>\n                                    <td class=\" \">121000208 <i class=\"success fa fa-long-arrow-up\"></i>\n                                    </td>\n                                    <td class=\" \">John Blank L</td>\n                                    <td class=\" \">Paid</td>\n                                    <td class=\"a-right a-right \">$741.20</td>\n                                    <td class=\" last\"><a href=\"#\">View</a>\n                                    </td>\n                                </tr>\n\n                            </tbody>\n                          </table>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/currency/list/list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/admin/currency/list/list.component.ts ***!
  \*******************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ListComponent = /** @class */ (function () {
    function ListComponent() {
    }
    ListComponent.prototype.ngOnInit = function () {
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/admin/currency/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/admin/currency/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ListComponent);
    return ListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=currency-currency-module.js.map