(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-user-module"],{

/***/ "./src/app/admin/user/list/list.component.css":
/*!****************************************************!*\
  !*** ./src/app/admin/user/list/list.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".last > a {\n      margin-right: 20px;\n}\n.ngx-pagination {\n    margin-left: 0;\n    margin-bottom: 1rem;\n    float: right !important;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vdXNlci9saXN0L2xpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtNQUNNLGtCQUFrQjtBQUN4QjtBQUNBO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQix1QkFBdUI7QUFDM0IiLCJmaWxlIjoic3JjL2FwcC9hZG1pbi91c2VyL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxhc3QgPiBhIHtcbiAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcbn1cbi5uZ3gtcGFnaW5hdGlvbiB7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgICBmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/admin/user/list/list.component.html":
/*!*****************************************************!*\
  !*** ./src/app/admin/user/list/list.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>All Users </h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>All Users <small>Users list</small></h2>\n                        <div class=\"clearfix\"></div>\n                    </div>\n\n                    <div class=\"x_content\">\n                        <div class=\"table-responsive\">\n                            <table class=\"table table-striped jambo_table bulk_action\">\n                                <thead>\n                                    <tr class=\"headings\">\n                                        <th>\n                                          <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                        </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">User Name </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">User Email</th>\n                                      \n                                        \n                                       <!--  <th class=\"column-title no-link last\" style=\"display: table-cell;\"><span class=\"nobr\">Action</span>\n                                        </th> -->\n                                        <th class=\"bulk-actions\" colspan=\"7\" style=\"display: none;\">\n                                            <a class=\"antoo\" style=\"color:#fff; font-weight:500;\">Bulk Actions ( <span class=\"action-cnt\">1 Records Selected</span> ) <i class=\"fa fa-chevron-down\"></i></a>\n                                        </th>\n                                    </tr>\n                                </thead>\n\n                                <tbody>\n                                    <tr class=\"even pointer\" *ngFor=\"let listItem of listItems | paginate: { itemsPerPage: 2, currentPage: p }\">\n                                        <td class=\"a-center \">\n                                            <input type=\"checkbox\"  name=\"{{ listItem._id }}\" style=\"position: absolute;\">\n                                        </td>\n                                        <td class=\" \">{{ listItem.name }}</td>\n                                        <td class=\" \">{{ listItem.email }}</td>\n                                        \n                                       <!--  <td class=\" last\"><a [routerLink]=\"['/admin/cms/edit', listItem._id]\"><i class=\"fa fa-edit\"></i></a>\n                                        <button class=\"btn btn-default\" mwlConfirmationPopover [popoverTitle]=\"popoverTitle\"   [popoverMessage]=\"popoverMessage\" placement=\"top\" (confirm)=\"deleteCms(listItem._id)\" (cancel)=\"cancelClicked = true\"> <i class=\"fa fa-trash-o\"></i></button>\n\n                                        </td> -->\n                                    </tr>\n                                    <tr><td colspan=\"3\"><pagination-controls (pageChange)=\"p = $event\" [class]=\"'ngx-pagination'\"></pagination-controls></td></tr>\n                                </tbody>\n                            </table>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/user/list/list.component.ts":
/*!***************************************************!*\
  !*** ./src/app/admin/user/list/list.component.ts ***!
  \***************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");






var ListComponent = /** @class */ (function () {
    function ListComponent(_router, _adminservice, toastr) {
        this._router = _router;
        this._adminservice = _adminservice;
        this.toastr = toastr;
        this.listItems = [];
        this.serverError = '';
        this.popoverTitle = 'Are you sure';
        this.popoverMessage = 'You want to delete this cms';
        this.p = 1;
    }
    ListComponent.prototype.ngOnInit = function () {
        this.fetchData();
    };
    ListComponent.prototype.fetchData = function () {
        var _this = this;
        this._adminservice.getAllUsers()
            .subscribe(function (res) {
            _this.listItems = res.result.users;
            console.log(_this.listItems);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/admin/user/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/admin/user/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/admin/user/user-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/admin/user/user-routing.module.ts ***!
  \***************************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/user/list/list.component.ts");




var routes = [
    { path: '', component: _list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"] },
    // {path: 'add', component: AddComponent},
    // {path: 'edit/:id', component: EditComponent},
    { path: 'list', component: _list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"] },
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/user/user.module.ts":
/*!*******************************************!*\
  !*** ./src/app/admin/user/user.module.ts ***!
  \*******************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _user_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-routing.module */ "./src/app/admin/user/user-routing.module.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/user/list/list.component.ts");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular-confirmation-popover */ "./node_modules/angular-confirmation-popover/fesm5/angular-confirmation-popover.js");




//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';






var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_list_list_component__WEBPACK_IMPORTED_MODULE_7__["ListComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _user_routing_module__WEBPACK_IMPORTED_MODULE_6__["UserRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_5__["NgxPaginationModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_8__["CKEditorModule"],
                //BrowserAnimationsModule, // required animations module
                ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"].forRoot(),
                angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_9__["ConfirmationPopoverModule"].forRoot({
                    confirmButtonType: 'danger' // set defaults here
                })
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ })

}]);
//# sourceMappingURL=user-user-module.js.map