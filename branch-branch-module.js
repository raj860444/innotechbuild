(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["branch-branch-module"],{

/***/ "./src/app/admin/branch/add/add.component.css":
/*!****************************************************!*\
  !*** ./src/app/admin/branch/add/add.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2JyYW5jaC9hZGQvYWRkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/branch/add/add.component.html":
/*!*****************************************************!*\
  !*** ./src/app/admin/branch/add/add.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>Form Elements</h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n               <div class=\"x_panel\">\n                 <div class=\"x_title\">\n                   <h2>Form Design <small>different form elements</small></h2>\n                   <ul class=\"nav navbar-right panel_toolbox\">\n                     <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n                     </li>\n                     <li class=\"dropdown\">\n                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n                       <ul class=\"dropdown-menu\" role=\"menu\">\n                         <li><a href=\"#\">Settings 1</a>\n                         </li>\n                         <li><a href=\"#\">Settings 2</a>\n                         </li>\n                       </ul>\n                     </li>\n                     <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n                     </li>\n                   </ul>\n                   <div class=\"clearfix\"></div>\n                 </div>\n                 <div class=\"x_content\">\n                   <br />\n                   You have following {{serverError }}\n                   <form [formGroup]=\"addBranchForm\" (ngSubmit)=\"addBranch()\" id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">\n                     <div class=\"form-group\">\n                       <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Branch Name <span class=\"required\">*</span>\n                       </label>\n                       <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                         <input type=\"text\" formControlName=\"branchName\" id=\"first-name\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                       </div>\n                     </div>\n                     <div class=\"form-group\">\n                       <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Complete Branch Address <span class=\"required\">*</span>\n                       </label>\n                       <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                         <input type=\"text\" formControlName=\"branchAddress\" id=\"last-name\" name=\"last-name\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                       </div>\n                     </div>\n                     <div class=\"ln_solid\"></div>\n                     <div class=\"form-group\">\n                       <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">\n                         <a routerLink=\"/admin/branch/list\" class=\"btn btn-primary\" type=\"button\">Cancel</a>\n                         <button class=\"btn btn-primary\" type=\"reset\">Reset</button>\n                         <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addBranchForm.valid\">Submit</button>\n                       </div>\n                     </div>\n                   </form>\n                 </div>\n               </div>\n             </div>\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/branch/add/add.component.ts":
/*!***************************************************!*\
  !*** ./src/app/admin/branch/add/add.component.ts ***!
  \***************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var AddComponent = /** @class */ (function () {
    function AddComponent(_auth, _adminservice, _router, fb, toastr) {
        this._auth = _auth;
        this._adminservice = _adminservice;
        this._router = _router;
        this.fb = fb;
        this.toastr = toastr;
        this.serverError = '';
        // addBranchForm = new FormGroup({
        //   branchName: new FormControl(''),
        //   branchAddress: new FormControl(''),
        // });
        this.addBranchForm = this.fb.group({
            branchName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            branchAddress: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    AddComponent.prototype.ngOnInit = function () {
    };
    AddComponent.prototype.addBranch = function () {
        var _this = this;
        this._adminservice.addNewBranch(this.addBranchForm.value)
            .subscribe(function (res) {
            _this.toastr.success('Branch Added Successfully', 'Success!');
            _this._router.navigate(['/admin/branch/list']);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
        console.log(this.addBranchForm.value);
    };
    AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add',
            template: __webpack_require__(/*! ./add.component.html */ "./src/app/admin/branch/add/add.component.html"),
            styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/admin/branch/add/add.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _admin_service__WEBPACK_IMPORTED_MODULE_5__["AdminService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], AddComponent);
    return AddComponent;
}());



/***/ }),

/***/ "./src/app/admin/branch/branch-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/admin/branch/branch-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: BranchRoutingModule, ComponentList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchRoutingModule", function() { return BranchRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentList", function() { return ComponentList; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/add.component */ "./src/app/admin/branch/add/add.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/branch/list/list.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/admin/branch/edit/edit.component.ts");






var routes = [
    { path: '', 'redirectTo': '/list', pathMatch: 'full' },
    { path: 'list', component: _list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"] },
    { path: 'add', component: _add_add_component__WEBPACK_IMPORTED_MODULE_3__["AddComponent"] },
    { path: 'edit/:id', component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_5__["EditComponent"] },
];
var BranchRoutingModule = /** @class */ (function () {
    function BranchRoutingModule() {
    }
    BranchRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], BranchRoutingModule);
    return BranchRoutingModule;
}());

var ComponentList = [_add_add_component__WEBPACK_IMPORTED_MODULE_3__["AddComponent"], _list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"], _edit_edit_component__WEBPACK_IMPORTED_MODULE_5__["EditComponent"]];


/***/ }),

/***/ "./src/app/admin/branch/branch.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/branch/branch.module.ts ***!
  \***********************************************/
/*! exports provided: BranchModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchModule", function() { return BranchModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _branch_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./branch-routing.module */ "./src/app/admin/branch/branch-routing.module.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");





//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


var BranchModule = /** @class */ (function () {
    function BranchModule() {
    }
    BranchModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_branch_routing_module__WEBPACK_IMPORTED_MODULE_4__["ComponentList"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _branch_routing_module__WEBPACK_IMPORTED_MODULE_4__["BranchRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_6__["NgxPaginationModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                //BrowserAnimationsModule, // required animations module
                ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"].forRoot() // ToastrModule added
            ]
        })
    ], BranchModule);
    return BranchModule;
}());



/***/ }),

/***/ "./src/app/admin/branch/edit/edit.component.css":
/*!******************************************************!*\
  !*** ./src/app/admin/branch/edit/edit.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2JyYW5jaC9lZGl0L2VkaXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/branch/edit/edit.component.html":
/*!*******************************************************!*\
  !*** ./src/app/admin/branch/edit/edit.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>Form Elements</h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n               <div class=\"x_panel\">\n                 <div class=\"x_title\">\n                   <h2>Form Design <small>different form elements</small></h2>\n                   <ul class=\"nav navbar-right panel_toolbox\">\n                     <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n                     </li>\n                     <li class=\"dropdown\">\n                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n                       <ul class=\"dropdown-menu\" role=\"menu\">\n                         <li><a href=\"#\">Settings 1</a>\n                         </li>\n                         <li><a href=\"#\">Settings 2</a>\n                         </li>\n                       </ul>\n                     </li>\n                     <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n                     </li>\n                   </ul>\n                   <div class=\"clearfix\"></div>\n                 </div>\n                 <div class=\"x_content\">\n                   <br />\n                   You have following {{serverError }}\n                   <form [formGroup]=\"editBranchForm\" (ngSubmit)=\"editBranch()\" id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">\n                     <div class=\"form-group\">\n                       <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Branch Name <span class=\"required\">*</span>\n                       </label>\n                       <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                         <input type=\"text\" formControlName=\"branchName\" id=\"first-name\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                       </div>\n                     </div>\n                     <div class=\"form-group\">\n                       <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Complete Branch Address <span class=\"required\">*</span>\n                       </label>\n                       <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                         <input type=\"text\" formControlName=\"branchAddress\" id=\"last-name\" name=\"last-name\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                       </div>\n                     </div>\n                     <div class=\"ln_solid\"></div>\n                     <div class=\"form-group\">\n                       <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">\n                         <a routerLink=\"/admin/branch/list\" class=\"btn btn-primary\" type=\"button\">Cancel</a>\n                         <button class=\"btn btn-primary\" type=\"reset\">Reset</button>\n                         <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!editBranchForm.valid\">Submit</button>\n                       </div>\n                     </div>\n                   </form>\n                 </div>\n               </div>\n             </div>\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/branch/edit/edit.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/admin/branch/edit/edit.component.ts ***!
  \*****************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");








var EditComponent = /** @class */ (function () {
    function EditComponent(_router, activatedRoute, _http, _adminservice, fb, toastr) {
        this._router = _router;
        this.activatedRoute = activatedRoute;
        this._http = _http;
        this._adminservice = _adminservice;
        this.fb = fb;
        this.toastr = toastr;
        this.data = [];
        this.editBranchForm = this.fb.group({
            branchName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            branchAddress: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
        });
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        this._adminservice.getSingleBranch(this.id).subscribe(function (res) {
            console.log(res);
            _this.data = res.createdBranch;
            _this.editBranchForm.patchValue({
                branchName: res.createdBranch.branchName,
                branchAddress: res.createdBranch.branchAddress
            });
        }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpErrorResponse"]) {
                if (err.status === 404) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 500) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
        //console.log(this.id)
    };
    EditComponent.prototype.editBranch = function () {
        var _this = this;
        console.log(this.editBranchForm.value);
        console.log("id is: " + this.id);
        this._adminservice.updateBranch(this.id, this.editBranchForm.value).subscribe(function (res) {
            _this.toastr.success('Branch Updated Successfully', 'Success!');
            _this._router.navigate(['/admin/branch/list']);
        }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpErrorResponse"]) {
                if (err.status === 404) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 500) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! ./edit.component.html */ "./src/app/admin/branch/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/admin/branch/edit/edit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _admin_service__WEBPACK_IMPORTED_MODULE_4__["AdminService"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/admin/branch/list/list.component.css":
/*!******************************************************!*\
  !*** ./src/app/admin/branch/list/list.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".last > a {\n      margin-right: 20px;\n}\n.ngx-pagination {\n    margin-left: 0;\n    margin-bottom: 1rem;\n    float: right !important;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vYnJhbmNoL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO01BQ00sa0JBQWtCO0FBQ3hCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLHVCQUF1QjtBQUMzQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2JyYW5jaC9saXN0L2xpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sYXN0ID4gYSB7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4ubmd4LXBhZ2luYXRpb24ge1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XG4gICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/admin/branch/list/list.component.html":
/*!*******************************************************!*\
  !*** ./src/app/admin/branch/list/list.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>All Branches</h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>All Branches <small>Custom design</small></h2>\n                        <div class=\"clearfix\"></div>\n                    </div>\n\n                    <div class=\"x_content\">\n                        <div class=\"table-responsive\">\n                            <table class=\"table table-striped jambo_table bulk_action\">\n                                <thead>\n                                    <tr class=\"headings\">\n                                        <th>\n                                          <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                        </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">Branch Name </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">Branch Address </th>\n                                        <th class=\"column-title no-link last\" style=\"display: table-cell;\"><span class=\"nobr\">Action</span>\n                                        </th>\n                                        <th class=\"bulk-actions\" colspan=\"7\" style=\"display: none;\">\n                                            <a class=\"antoo\" style=\"color:#fff; font-weight:500;\">Bulk Actions ( <span class=\"action-cnt\">1 Records Selected</span> ) <i class=\"fa fa-chevron-down\"></i></a>\n                                        </th>\n                                    </tr>\n                                </thead>\n\n                                <tbody>\n                                    <tr class=\"even pointer\" *ngFor=\"let listItem of listItems | paginate: { itemsPerPage: 2, currentPage: p }\">\n                                        <td class=\"a-center \">\n                                            <input type=\"checkbox\"  name=\"{{ listItem._id }}\" style=\"position: absolute;\">\n                                        </td>\n                                        <td class=\" \">{{ listItem.branchName }}</td>\n                                        <td class=\" \">{{ listItem.branchAddress }}</td>\n                                        <td class=\" last\"><a [routerLink]=\"['/admin/branch/edit', listItem._id]\"><i class=\"fa fa-edit\"></i></a><a (click)=\"deleteBranch(listItem._id)\"><i class=\"fa fa-trash-o\"></i></a>\n                                        </td>\n                                    </tr>\n                                    <tr><td colspan=\"4\"><pagination-controls (pageChange)=\"p = $event\" [class]=\"'ngx-pagination'\"></pagination-controls></td></tr>\n                                </tbody>\n                            </table>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/branch/list/list.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/admin/branch/list/list.component.ts ***!
  \*****************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");






var ListComponent = /** @class */ (function () {
    function ListComponent(_router, _adminservice, toastr) {
        this._router = _router;
        this._adminservice = _adminservice;
        this.toastr = toastr;
        this.listItems = [];
        this.serverError = '';
        this.p = 1;
    }
    ListComponent.prototype.ngOnInit = function () {
        this.fetchData();
    };
    ListComponent.prototype.fetchData = function () {
        var _this = this;
        this._adminservice.getAllBranch()
            .subscribe(function (res) {
            _this.listItems = res.result.branches;
            console.log(_this.listItems);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    ListComponent.prototype.deleteBranch = function (id) {
        var _this = this;
        console.log(id);
        this._adminservice.deleteBranch(id).subscribe(function (res) {
            _this.toastr.success('Branch Deleted Successfully', 'Success :)');
            _this.fetchData();
        }, function (err) {
            if (err.status === 500) {
                console.log(err);
                _this.serverError = err.error;
                _this.toastr.error(err.error, '!Error');
            }
            else {
                _this.toastr.error('Unknown error please check you input and try again', '!Error');
            }
        });
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/admin/branch/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/admin/branch/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=branch-branch-module.js.map