(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job-job-module"],{

/***/ "./src/app/admin/job/add/add.component.css":
/*!*************************************************!*\
  !*** ./src/app/admin/job/add/add.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .ck-editor__editable {\n    min-height: 200px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vam9iL2FkZC9hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2pvYi9hZGQvYWRkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCA6Om5nLWRlZXAgLmNrLWVkaXRvcl9fZWRpdGFibGUge1xuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/admin/job/add/add.component.html":
/*!**************************************************!*\
  !*** ./src/app/admin/job/add/add.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n<div class=\"right_col\" role=\"main\">\n    <div class=\"\">\n        <div class=\"page-title\">\n            <div class=\"title_left\">\n                <h3>Add Job</h3>\n            </div>\n\n            <div class=\"title_right\">\n                <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                    <div class=\"input-group\">\n                        <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                        <span class=\"input-group-btn\">\n                      <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                    </span>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"></div>\n        <div class=\"row\">\n            <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>Add Job <small>Add New Job</small></h2>\n                        <ul class=\"nav navbar-right panel_toolbox\">\n                            <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n                            </li>\n                            <li class=\"dropdown\">\n                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n                                <ul class=\"dropdown-menu\" role=\"menu\">\n                                    <li><a href=\"#\">Settings 1</a>\n                                    </li>\n                                    <li><a href=\"#\">Settings 2</a>\n                                    </li>\n                                </ul>\n                            </li>\n                            <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n                            </li>\n                        </ul>\n                        <div class=\"clearfix\"></div>\n                    </div>\n                    <div class=\"x_content\">\n                        <br />\n                        <form [formGroup]=\"addJobForm\" (ngSubmit)=\"onSubmit()\" id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">\n\n                            <div class=\"form-group\">\n                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Job Title<span class=\"required\">*</span>\n                                </label>\n                                <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                                    <input type=\"text\" formControlName=\"job_title\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                                    <div *ngIf=\"job_title.invalid && (job_title.dirty || job_title.touched)\" class=\"alert alert-danger\">Job Title is required.</div>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Job Description <span class=\"required\">*</span>\n                                </label>\n                                <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                                    <input type=\"text\" formControlName=\"job_description\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                                    <div *ngIf=\"job_description.invalid && (job_description.dirty || job_description.touched)\" class=\"alert alert-danger\">Job Description is required.</div>\n                                </div>\n                            </div>\n                            <div class=\"ln_solid\"></div>\n                            <div class=\"form-group\">\n                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">\n                                    <button class=\"btn btn-primary\" type=\"button\">Cancel</button>\n                                    <button class=\"btn btn-primary\" type=\"reset\">Reset</button>\n                                    <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addJobForm.valid\">Submit</button>\n                                </div>\n                            </div>\n\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/job/add/add.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/job/add/add.component.ts ***!
  \************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__);









var AddComponent = /** @class */ (function () {
    function AddComponent(_auth, _adminservice, _router, fb, toastr) {
        this._auth = _auth;
        this._adminservice = _adminservice;
        this._router = _router;
        this.fb = fb;
        this.toastr = toastr;
        this.serverError = '';
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__;
        this.addJobForm = this.fb.group({
            job_title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            job_description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    AddComponent.prototype.ngOnInit = function () {
    };
    AddComponent.prototype.onSubmit = function () {
        var _this = this;
        console.warn(this.addJobForm.value);
        this._adminservice.addJob(this.addJobForm.value)
            .subscribe(function (result) {
            _this.toastr.success('Job Added Successfully', 'Success!');
            _this._router.navigate(['/admin/job/list']);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    Object.defineProperty(AddComponent.prototype, "job_title", {
        get: function () { return this.addJobForm.get('job_title'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AddComponent.prototype, "job_description", {
        get: function () { return this.addJobForm.get('job_description'); },
        enumerable: true,
        configurable: true
    });
    AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add',
            template: __webpack_require__(/*! ./add.component.html */ "./src/app/admin/job/add/add.component.html"),
            styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/admin/job/add/add.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _admin_service__WEBPACK_IMPORTED_MODULE_5__["AdminService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], AddComponent);
    return AddComponent;
}());



/***/ }),

/***/ "./src/app/admin/job/edit/edit.component.css":
/*!***************************************************!*\
  !*** ./src/app/admin/job/edit/edit.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2pvYi9lZGl0L2VkaXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/job/edit/edit.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin/job/edit/edit.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n<div class=\"right_col\" role=\"main\">\n    <div class=\"\">\n        <div class=\"page-title\">\n            <div class=\"title_left\">\n                <h3>Edit Job</h3>\n            </div>\n\n            <div class=\"title_right\">\n                <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                    <div class=\"input-group\">\n                        <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                        <span class=\"input-group-btn\">\n                      <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                    </span>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"></div>\n        <div class=\"row\">\n            <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>Edit Job <small>Edit New Job</small></h2>\n                        <ul class=\"nav navbar-right panel_toolbox\">\n                            <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n                            </li>\n                            <li class=\"dropdown\">\n                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n                                <ul class=\"dropdown-menu\" role=\"menu\">\n                                    <li><a href=\"#\">Settings 1</a>\n                                    </li>\n                                    <li><a href=\"#\">Settings 2</a>\n                                    </li>\n                                </ul>\n                            </li>\n                            <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n                            </li>\n                        </ul>\n                        <div class=\"clearfix\"></div>\n                    </div>\n                    <div class=\"x_content\">\n                        <br />\n                        <form [formGroup]=\"editJobForm\" (ngSubmit)=\"editJob()\" id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">\n\n                            <div class=\"form-group\">\n                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Job Title<span class=\"required\">*</span>\n                                </label>\n                                <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                                    <input type=\"text\" formControlName=\"job_title\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                                    <div *ngIf=\"job_title.invalid && (job_title.dirty || job_title.touched)\" class=\"alert alert-danger\">Job Title is required.</div>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Job Description <span class=\"required\">*</span>\n                                </label>\n                                <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                                    <input type=\"text\" formControlName=\"job_description\" required=\"required\" class=\"form-control col-md-7 col-xs-12\">\n                                    <div *ngIf=\"job_description.invalid && (job_description.dirty || job_description.touched)\" class=\"alert alert-danger\">Job Description is required.</div>\n                                </div>\n                            </div>\n                            <div class=\"ln_solid\"></div>\n                            <div class=\"form-group\">\n                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">\n                                    <button class=\"btn btn-primary\" type=\"button\">Cancel</button>\n                                    <button class=\"btn btn-primary\" type=\"reset\">Reset</button>\n                                    <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!editJobForm.valid\">Submit</button>\n                                </div>\n                            </div>\n\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/job/edit/edit.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/job/edit/edit.component.ts ***!
  \**************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_7__);









var EditComponent = /** @class */ (function () {
    function EditComponent(_router, activatedRoute, _http, _adminservice, fb, toastr) {
        this._router = _router;
        this.activatedRoute = activatedRoute;
        this._http = _http;
        this._adminservice = _adminservice;
        this.fb = fb;
        this.toastr = toastr;
        this.data = [];
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_7__;
        this.editJobForm = this.fb.group({
            job_title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            job_description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
        });
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        console.log(this.id);
        this._adminservice.getSingleJob(this.id).subscribe(function (res) {
            console.log(res);
            _this.data = res.createdJob;
            _this.editJobForm.patchValue({
                job_title: res.createdJob.job_title,
                job_description: res.createdJob.job_description
            });
        }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpErrorResponse"]) {
                if (err.status === 404) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 500) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
        //console.log(this.id)
    };
    Object.defineProperty(EditComponent.prototype, "job_title", {
        get: function () { return this.editJobForm.get('job_title'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditComponent.prototype, "job_description", {
        get: function () { return this.editJobForm.get('job_description'); },
        enumerable: true,
        configurable: true
    });
    EditComponent.prototype.editJob = function () {
        var _this = this;
        console.log(this.editJobForm.value);
        console.log("id is: " + this.id);
        this._adminservice.updateJob(this.id, this.editJobForm.value).subscribe(function (res) {
            _this.toastr.success('Job Updated Successfully', 'Success!');
            _this._router.navigate(['/admin/job/list']);
        }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpErrorResponse"]) {
                if (err.status === 404) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 500) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! ./edit.component.html */ "./src/app/admin/job/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/admin/job/edit/edit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _admin_service__WEBPACK_IMPORTED_MODULE_4__["AdminService"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/admin/job/job-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/admin/job/job-routing.module.ts ***!
  \*************************************************/
/*! exports provided: JobRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobRoutingModule", function() { return JobRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/add.component */ "./src/app/admin/job/add/add.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/admin/job/edit/edit.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/job/list/list.component.ts");






var routes = [
    { path: '', component: _list_list_component__WEBPACK_IMPORTED_MODULE_5__["ListComponent"] },
    { path: 'add', component: _add_add_component__WEBPACK_IMPORTED_MODULE_3__["AddComponent"] },
    { path: 'edit/:id', component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_4__["EditComponent"] },
    { path: 'list', component: _list_list_component__WEBPACK_IMPORTED_MODULE_5__["ListComponent"] },
];
var JobRoutingModule = /** @class */ (function () {
    function JobRoutingModule() {
    }
    JobRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], JobRoutingModule);
    return JobRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/job/job.module.ts":
/*!*****************************************!*\
  !*** ./src/app/admin/job/job.module.ts ***!
  \*****************************************/
/*! exports provided: JobModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobModule", function() { return JobModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _job_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./job-routing.module */ "./src/app/admin/job/job-routing.module.ts");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add/add.component */ "./src/app/admin/job/add/add.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/admin/job/edit/edit.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list/list.component */ "./src/app/admin/job/list/list.component.ts");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-confirmation-popover */ "./node_modules/angular-confirmation-popover/fesm5/angular-confirmation-popover.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");










//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


var JobModule = /** @class */ (function () {
    function JobModule() {
    }
    JobModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_add_add_component__WEBPACK_IMPORTED_MODULE_4__["AddComponent"], _edit_edit_component__WEBPACK_IMPORTED_MODULE_5__["EditComponent"], _list_list_component__WEBPACK_IMPORTED_MODULE_6__["ListComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _job_routing_module__WEBPACK_IMPORTED_MODULE_3__["JobRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_11__["NgxPaginationModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_7__["CKEditorModule"],
                //BrowserAnimationsModule, // required animations module
                ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrModule"].forRoot(),
                angular_confirmation_popover__WEBPACK_IMPORTED_MODULE_8__["ConfirmationPopoverModule"].forRoot({
                    confirmButtonType: 'danger' // set defaults here
                })
            ]
        })
    ], JobModule);
    return JobModule;
}());



/***/ }),

/***/ "./src/app/admin/job/list/list.component.css":
/*!***************************************************!*\
  !*** ./src/app/admin/job/list/list.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".last > a {\n      margin-right: 20px;\n}\n.ngx-pagination {\n    margin-left: 0;\n    margin-bottom: 1rem;\n    float: right !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRtaW4vam9iL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO01BQ00sa0JBQWtCO0FBQ3hCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLHVCQUF1QjtBQUMzQiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2pvYi9saXN0L2xpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sYXN0ID4gYSB7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG4ubmd4LXBhZ2luYXRpb24ge1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XG4gICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/admin/job/list/list.component.html":
/*!****************************************************!*\
  !*** ./src/app/admin/job/list/list.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- page content -->\n       <div class=\"right_col\" role=\"main\">\n         <div class=\"\">\n           <div class=\"page-title\">\n             <div class=\"title_left\">\n               <h3>All Jobs </h3>\n             </div>\n\n             <div class=\"title_right\">\n               <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n                 <div class=\"input-group\">\n                   <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\">\n                   <span class=\"input-group-btn\">\n                     <button class=\"btn btn-default\" type=\"button\">Go!</button>\n                   </span>\n                 </div>\n               </div>\n             </div>\n           </div>\n           <div class=\"clearfix\"></div>\n           <div class=\"row\">\n             <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"x_panel\">\n                    <div class=\"x_title\">\n                        <h2>All Jobs <small>Jobs list</small></h2>\n                        <div class=\"clearfix\"></div>\n                    </div>\n\n                    <div class=\"x_content\">\n                        <div class=\"table-responsive\">\n                            <table class=\"table table-striped jambo_table bulk_action\">\n                                <thead>\n                                    <tr class=\"headings\">\n                                        <th>\n                                          <input type=\"checkbox\" id=\"check-all\" class=\"flat\">\n                                        </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">Job Title </th>\n                                        <th class=\"column-title\" style=\"display: table-cell;\">Job Description </th>\n                                       <th class=\"column-title no-link last\" style=\"display: table-cell;\"><span class=\"nobr\">Action</span>\n                                        </th>\n                                        <th class=\"bulk-actions\" colspan=\"7\" style=\"display: none;\">\n                                            <a class=\"antoo\" style=\"color:#fff; font-weight:500;\">Bulk Actions ( <span class=\"action-cnt\">1 Records Selected</span> ) <i class=\"fa fa-chevron-down\"></i></a>\n                                        </th>\n                                    </tr>\n                                </thead>\n\n                                <tbody>\n                                    <tr class=\"even pointer\" *ngFor=\"let listItem of listItems | paginate: { itemsPerPage: 2, currentPage: p }\">\n                                        <td class=\"a-center \">\n                                            <input type=\"checkbox\"  name=\"{{ listItem._id }}\" style=\"position: absolute;\">\n                                        </td>\n                                        <td class=\" \">{{ listItem.job_title }}</td>\n                                        <td class=\" \">{{ listItem.job_description }}</td>\n                                        <td class=\" last\"><a [routerLink]=\"['/admin/job/edit', listItem._id]\"><i class=\"fa fa-edit\"></i></a>\n                                        <button class=\"btn btn-default\" mwlConfirmationPopover [popoverTitle]=\"popoverTitle\"   [popoverMessage]=\"popoverMessage\" placement=\"top\" (confirm)=\"deleteJob(listItem._id)\" (cancel)=\"cancelClicked = true\"> <i class=\"fa fa-trash-o\"></i></button>\n\n                                          <!-- <a (click)=\"deleteCms(listItem._id)\"><i class=\"fa fa-trash-o\"></i></a> -->\n                                        </td>\n                                    </tr>\n                                    <tr><td colspan=\"4\"><pagination-controls (pageChange)=\"p = $event\" [class]=\"'ngx-pagination'\"></pagination-controls></td></tr>\n                                </tbody>\n                            </table>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n           </div>\n         </div>\n       </div>\n       <!-- /page content -->\n"

/***/ }),

/***/ "./src/app/admin/job/list/list.component.ts":
/*!**************************************************!*\
  !*** ./src/app/admin/job/list/list.component.ts ***!
  \**************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");






var ListComponent = /** @class */ (function () {
    function ListComponent(_router, _adminservice, toastr) {
        this._router = _router;
        this._adminservice = _adminservice;
        this.toastr = toastr;
        this.listItems = [];
        this.serverError = '';
        this.popoverTitle = 'Are you sure';
        this.popoverMessage = 'You want to delete this job';
        this.p = 1;
    }
    ListComponent.prototype.ngOnInit = function () {
        this.fetchData();
    };
    ListComponent.prototype.fetchData = function () {
        var _this = this;
        this._adminservice.getAllJob()
            .subscribe(function (res) {
            console.log(res);
            _this.listItems = res.result.Job;
            console.log(_this.listItems);
        }, function (err) {
            console.log(err);
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpErrorResponse"]) {
                if (err.status === 409) {
                    _this.serverError = err.error.message;
                }
                if (err.status === 401) {
                    _this.serverError = 'Unauthorization Error plz logout and login again';
                }
            }
        });
    };
    ListComponent.prototype.deleteJob = function (id) {
        var _this = this;
        console.log(id);
        this._adminservice.deleteJob(id).subscribe(function (res) {
            _this.toastr.success('Job Deleted Successfully', 'Success :)');
            _this.fetchData();
        }, function (err) {
            if (err.status === 500) {
                console.log(err);
                _this.serverError = err.error;
                _this.toastr.error(err.error, '!Error');
            }
            else {
                _this.toastr.error('Unknown error please check you input and try again', '!Error');
            }
        });
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/admin/job/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/admin/job/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=job-job-module.js.map